﻿using System;

[Serializable]
public class CharacterData
{
    public int MaxHealth = 10;

    public int Health = 10;

    public int Lifes = 3;

    public float AttackDistance = 0.5f;

    public int AttackPoints = 1;
}
