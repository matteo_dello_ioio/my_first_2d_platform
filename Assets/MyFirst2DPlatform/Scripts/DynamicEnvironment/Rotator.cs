﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{

    public enum RotationAxis
    {
        X, Y, Z
    }

    [SerializeField]
    int _rotationSpeed;

    [SerializeField]
    RotationAxis Axis;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        Vector3 rotationAxis = Vector3.zero;
        switch(Axis)
        {
            case RotationAxis.X:
                rotationAxis = Vector3.right;
                break;
            case RotationAxis.Y:
                rotationAxis = Vector3.up;
                break;
            case RotationAxis.Z:
                rotationAxis = Vector3.forward;
                break;
        }
        transform.Rotate(rotationAxis, _rotationSpeed * Time.deltaTime);
	}
}
