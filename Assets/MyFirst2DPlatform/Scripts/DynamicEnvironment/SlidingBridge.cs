﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyFirst2DPlatform.Mechanics
{
    [RequireComponent(typeof(SliderJoint2D))]
    public class SlidingBridge : MonoBehaviour
    {
        enum State
        {
            Stopped,
            Moving,
            InvertingDirection
        }

        State _state;

        SliderJoint2D _sliderJoint;

        [SerializeField]
        float MaxDisplacement = 5;

        private void Awake()
        {
            _sliderJoint = GetComponent<SliderJoint2D>();
        }

        // Use this for initialization
        void Start()
        {

        }

        private void Update()
        {
            if (_state != State.InvertingDirection)
            {
                if (_sliderJoint.motor.motorSpeed == 0)
                    _state = State.Stopped;
                else
                    _state = State.Moving;
            }
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            //Debug.Log("_sliderJoint.jointTranslation " + _sliderJoint.jointTranslation);
            if (_state == State.InvertingDirection)
            {
                if (MaxDisplacement - Mathf.Abs(_sliderJoint.jointTranslation) > float.Epsilon)
                    _state = State.Moving;
            }
            else
                if (Mathf.Abs(_sliderJoint.jointTranslation) >= MaxDisplacement)
                InvertSlideDirection();
        }

        private void InvertSlideDirection()
        {
            _state = State.InvertingDirection;

            JointMotor2D motor = _sliderJoint.motor;
            motor.motorSpeed *= -1;
            _sliderJoint.motor = motor;
        }
    }
}