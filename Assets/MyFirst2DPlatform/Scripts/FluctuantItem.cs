﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyFirst2DPlatform
{
    public class FluctuantItem : MonoBehaviour
    {

        [SerializeField]
        float Displacement;

        Vector3 startPosition;

        [SerializeField]
        [Range(0, 10)]
        float Speed;

        // Use this for initialization
        void Start()
        {
            startPosition = transform.position;
        }

        // Update is called once per frame
        void Update()
        {
            Vector3 position = startPosition;
            position.y += Mathf.Sin(Time.time * Speed) * Displacement;
            transform.position = position;
        }
    }
}