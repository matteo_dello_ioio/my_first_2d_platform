﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyFirst2DPlatform.Mechanics
{
    public class KillZone : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.tag == "Player")
            {
                DamageInfo damage = new DamageInfo();
                damage.InstaKill = true;
                collision.gameObject.SendMessage("__TAKE_DAMAGE__", damage);
            }
        }
    }
}