﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace MyFirst2DPlatform
{
    public class GameManager : MonoBehaviour
    {
        GameObject _playerGameObject;
        Guybrush _guybrush;

        AudioSource _audioSource;

        int _coinsCollected;

        [SerializeField]
        Text _coinCounterText;

        [SerializeField]
        GameObject _lifePanel;

        [SerializeField]
        UIHealthBar _healthBar;
        UILifeBar _lifeBar;

        [SerializeField]
        GameObject _gameOverPanel;

        [SerializeField]
        AudioClip _gameOverAudioClip;

        [SerializeField]
        private float _playerRespawnTime = 1.5f;


        [SerializeField]
        Transform StartCheckPoint;
        [SerializeField]
        Transform CheckPoint;


        string _currentLevel;


        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
        }

        // Use this for initialization
        void Start()
        {
            _gameOverPanel.SetActive(false);

            _playerGameObject = GameObject.FindGameObjectWithTag("Player");
            _guybrush = _playerGameObject.GetComponent<Guybrush>();
            _playerGameObject.SetActive(false);

            if (_lifePanel == null)
                _lifePanel = GameObject.Find("LifePanel");
            _lifeBar = _lifePanel.GetComponent<UILifeBar>();

            if (_healthBar == null)
                _healthBar = GameObject.Find("HealthBar").GetComponent<UIHealthBar>();
            _healthBar.SetMaxValue(_guybrush.GetMaxHealth());
            _healthBar.Value = _guybrush.GetHealth();

            //if (_coinCounterText == null)
            //    throw new System.Exception("missing component");

            OnGameStart();
        }

        // Update is called once per frame
        void Update()
        {

        }


        public void RestartLevel()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        public void OnMainCharacterCollectCoin(Coin coin)
        {
            _coinsCollected += coin.Value;

#if UNITY_EDITOR
            if (_coinCounterText == null)
                Debug.LogWarning("text object is missing");
#endif
            _coinCounterText.text = _coinsCollected.ToString();

            _audioSource.PlayOneShot(coin.PickUpSound);
            Destroy(coin.gameObject);
        }

        /// <summary>
        /// Metodo che viene richiamato quando viene sollevato
        /// l'evento CheckpointEvent.
        /// </summary>
        public void OnCharacterGetCheckpoint(GameObject checkpoint)
        {
            CheckPoint = checkpoint.transform;
        }

        /// <summary>
        /// Gestione della sequenza di avvio del gioco.
        /// </summary>
        public void OnGameStart()
        {
            _playerGameObject.SetActive(true);
            CheckPoint = StartCheckPoint;
            RelocatePlayer(StartCheckPoint.position);
        }

        /// <summary>
        /// Gestisce l'evento quando l'energia del personaggio cambia.
        /// </summary>
        public void OnCharacterHealthChange()
        {
            _healthBar.Value = _guybrush.GetHealth();
        }

        /// <summary>
        /// Gestisce l'evento quando il personaggio perde una vita.
        /// </summary>
        public void OnCharacterLostLife()
        {
            _lifeBar.SetLifes(_guybrush.GetLifes());
            StartCoroutine(CharacterDeath());
        }

        /// <summary>
        /// Gestione della sequenza di fine gioco.
        /// </summary>
        public void OnGameOver()
        {
            Debug.Log("GAME OVER!!!");

            _gameOverPanel.SetActive(true);

            _lifeBar.SetLifes(0);
            if (!_audioSource.isPlaying)
            {
                _audioSource.PlayOneShot(_gameOverAudioClip);
            }
        }

        private IEnumerator CharacterDeath()
        {
            yield return new WaitForSeconds(_playerRespawnTime);

            if (_guybrush.GetLifes() > 0)
            {
                _guybrush.SendMessage("__RESPAWN__");
                RelocatePlayer(CheckPoint.position);
            }
        }

        private void RelocatePlayer(Vector3 position)
        {
            _playerGameObject.transform.position = position;
        }
    }
}