﻿using UnityEngine;
using UnityEngine.UI;

public class UIHealthBar : MonoBehaviour
{
    [SerializeField]
    Image ValueIndicator;

    int _unitValueWidth;

    [SerializeField]
    int MaxValue;

    public void SetMaxValue(int maxValue)
    {
        MaxValue = maxValue;
        UpdateUnitSize();
    }

    [SerializeField]
    public int Value;
    int _value = int.MinValue;

    // Use this for initialization
    void Start()
    {
        UpdateUnitSize();
    }

    // Update is called once per frame
    void Update()
    {
        if(_value != Value)
        {
            OnValueChanged();
        }
    }

    /// <summary>
    /// Ridimensiona la barra dell'energia in base ai punti vita del personaggio.
    /// </summary>
    void OnValueChanged()
    {
        _value = Value;

        Vector2 sizeDelta = ValueIndicator.rectTransform.sizeDelta;

        sizeDelta.x = _unitValueWidth * _value;

        ValueIndicator.rectTransform.sizeDelta = sizeDelta;
    }

    void UpdateUnitSize()
    {
        if(MaxValue != 0)
            _unitValueWidth = (int)ValueIndicator.rectTransform.rect.width / MaxValue;
    }

}
