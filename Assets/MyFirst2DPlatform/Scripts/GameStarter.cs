﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class GameStarter : MonoBehaviour
{

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    /// <summary>
    /// Start a new game. Load the first level.
    /// </summary>
    public void StartNewGame()
    {
        SceneManager.LoadScene("level0");
    }
}
