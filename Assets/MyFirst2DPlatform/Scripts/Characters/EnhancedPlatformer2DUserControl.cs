using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace MyFirst2DPlatform
{
    /// <summary>
    /// Gestione dell'input.
    /// </summary>
    [RequireComponent(typeof(MyPlatformerCharacter2D))]
    public class EnhancedPlatformer2DUserControl : MonoBehaviour
    {
        private MyPlatformerCharacter2D m_Character;

        private float _horizontal;
        public float Horizontal { get { return _horizontal; } }

        private bool _jump;
        public bool IsJumpingUp { get { return _jump; } set { _jump = value; } }

        private bool _jumpDown;
        public bool IsJumpingDown { get { return _jumpDown; } }

        private bool _crouch;
        public bool IsCrouch { get { return _crouch; } }

        private bool m_Attack;
        public bool IsAttacking { get { return m_Attack; } }


        Collision2D _platformCollision;


        private void Awake()
        {
            m_Character = GetComponent<MyPlatformerCharacter2D>();
        }


        private void Update()
        {
            if (!_jump)
            {
                m_Attack = CrossPlatformInputManager.GetButtonDown("Fire1");
                // Read the jump input in Update so button presses aren't missed.

                _jump = CrossPlatformInputManager.GetButtonDown("Jump");
                float v = CrossPlatformInputManager.GetAxis("Vertical");

                if (_jump && v < 0)
                {
                    _jumpDown = true;
                }
                else
                    _jumpDown = false;
            }

            if (_platformCollision != null && _jumpDown && _platformCollision.gameObject.tag == "pass-collider")
            {
                Physics2D.IgnoreCollision(_platformCollision.collider, gameObject.GetComponent<Collider2D>(), true);
            }
        }


        private void FixedUpdate()
        {
            // Read the inputs.
            _crouch = Input.GetKey(KeyCode.LeftControl);
            _horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
            // Pass all parameters to the character control script.
            //m_Character.Move(_horizontal, _crouch, _jump, _jumpDown, m_Attack, m_platformVelocity);
            //_jump = false;
            //m_platformVelocity = Vector2.zero;
        }


        private void OnCollisionEnter2D(Collision2D collision)
        {
            _platformCollision = collision;
        }

        private void OnCollisionExit2D(Collision2D collision)
        {
            _platformCollision = null;
        }
    }
}
