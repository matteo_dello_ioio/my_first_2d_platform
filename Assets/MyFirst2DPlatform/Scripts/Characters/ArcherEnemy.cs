﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyFirst2DPlatform
{

    public class ArcherEnemy : MonoBehaviour
    {
        CircleCollider2D _collider;
        Animator _animator;

        [SerializeField]
        int _lifePoints;

        [SerializeField]
        bool FlipOnStart;

        /// <summary>
        /// Distanza campo visivo.
        /// </summary>
        [SerializeField]
        float _viewDistance = 10;
        [SerializeField]
        float _viewRadius = 6;
        [SerializeField]
        private float _timeAfterDeath = 1;
        [SerializeField]
        private float _waitSecondsForNextAttack = 5;

        bool _doAttack = true;
        private bool m_FacingRight = true;  // For determining which way the player is currently facing.
        private bool _isDead;
        Coroutine _currentRoutine;

        GameObject _target;

        

        Transform _fireHole;

        [SerializeField]
        GameObject _arrowPrefab;

        [SerializeField]
        float FirePower = 500;


        private void Awake()
        {
            _animator = GetComponent<Animator>();
            _collider = GetComponent<CircleCollider2D>();
            _fireHole = transform.Find("firehole");
        }

        // Use this for initialization
        void Start()
        {
            if (FlipOnStart)
                Flip();

            _currentRoutine = StartCoroutine(IdleLogic());
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        public void __TAKE_DAMAGE__(DamageInfo damageInfo)
        {
            _lifePoints -= damageInfo.DamagePoint;
            if (_lifePoints > 0)
            {
                _animator.SetTrigger("hit");
            }
            else
            {
                if (!_isDead)
                {
                    SwitchRoutine(DeathLogic());
                }
            }
        }



        private void Flip()
        {
            // Switch the way the player is labelled as facing.
            m_FacingRight = !m_FacingRight;

            // Multiply the player's x local scale by -1.
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }


        void Attack()
        {
            _animator.SetTrigger("attack");
        }

        public void __SHOOT__()
        {
            GameObject newArrow = Instantiate<GameObject>(_arrowPrefab, _fireHole.position, Quaternion.identity);
            Physics2D.IgnoreCollision(newArrow.GetComponent<Collider2D>(), _collider);

            float randomness = Random.Range(0.9f, 1.1f);

            newArrow.GetComponent<Rigidbody2D>().AddForce(_fireHole.up * (FirePower * randomness));
        }


        //private void OnGUI()
        //{
        //    if (GUILayout.Button("HIT"))
        //    {
        //        gameObject.SendMessage("__TAKE_DAMAGE__");
        //    }

        //    if (GUILayout.Button("ATTACK"))
        //    {
        //        Attack();
        //    }
        //}

        /// <summary>
        /// Controlla se il giocatore rientra nel campo visivo.
        /// </summary>
        /// <param name="targets"></param>
        /// <returns></returns>
        bool CanSeePlayer(out List<GameObject> targets)
        {
            bool result = false;
            targets = null;

            Vector2 origin = transform.position;
            Vector2 direction = ((m_FacingRight) ? transform.right : -transform.right);
            int HIT_ARRAY_LENGTH = 10;
            RaycastHit2D[] hits = new RaycastHit2D[HIT_ARRAY_LENGTH];
            int hitCount = Physics2D.CircleCastNonAlloc(origin, _viewRadius, direction, hits, _viewDistance);

            //Debug.Log("TARGETS " + hitCount);

            if (hitCount > 0)
            {
                targets = new List<GameObject>();
                for (int i = 0; i < hitCount; i++)
                {
                    RaycastHit2D rh = hits[i];
                    GameObject hitGo = rh.collider.gameObject;
                    if (hitGo.tag == "Player")
                    {
                        _target = hitGo;

                        targets.Add(hitGo);
                        result = true;
                    }
                }
            }

            return result;
        }


        IEnumerator IdleLogic()
        {
            List<GameObject> targets = null;
            while (true)
            {
                if(CanSeePlayer(out targets))
                {
                    SwitchRoutine(AttackLogic());
                }

                yield return new WaitForSeconds(1);
            }
        }

        IEnumerator AttackLogic()
        {
            _doAttack = true;
            while (_doAttack)
            {
                if (
                    (_target.transform.position.x < gameObject.transform.position.x && m_FacingRight)
                    ||
                    (_target.transform.position.x > gameObject.transform.position.x && !m_FacingRight)
                    )
                    Flip();

                Attack();

                yield return new WaitForSeconds(_waitSecondsForNextAttack);
            }
        }

        IEnumerator DeathLogic()
        {
            _animator.SetTrigger("dead");
            _isDead = true;
            _collider.enabled = false;
            yield return new WaitForSeconds(_timeAfterDeath);
            Destroy(gameObject);
        }

        void SwitchRoutine(IEnumerator routine)
        {
            if (_currentRoutine != null)
            {
                StopCoroutine(_currentRoutine);
            }
            _currentRoutine = StartCoroutine(routine);
        }

    }
}