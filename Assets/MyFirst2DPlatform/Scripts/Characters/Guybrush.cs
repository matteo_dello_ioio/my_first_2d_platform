﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


namespace MyFirst2DPlatform
{
    /// <summary>
    /// Classe per evento custom.
    /// </summary>
    [Serializable]
    public class CheckpointEvent : UnityEvent<GameObject>
    {

    }

    /// <summary>
    /// Struttura per contenere le informazioni sul danno.
    /// </summary>
    public struct DamageInfo
    {
        /// <summary>
        /// Punti energia da sottrarre quando viene applicato il danno.
        /// </summary>
        public int DamagePoint;

        /// <summary>
        /// Indica che il personaggio deve perdere istantaneamente una vita
        /// indipendentemente dai punti energia che ha.
        /// </summary>
        public bool InstaKill;
    }


    //[Serializable]
    //public class CoinCollectedEvent : UnityEvent<Coin>
    //{

    //}

    public class Guybrush : MonoBehaviour
    {
        AudioSource _audioSource;

        [SerializeField]
        CapsuleCollider2D _bodyCollider;

        //[SerializeField]
        //public CoinCollectedEvent CoinCollected;

        [SerializeField]
        AudioClip JumpSound;

        GameObject _interactable;

        MyPlatformerCharacter2D _character;
        EnhancedPlatformer2DUserControl _input;

        [SerializeField]
        CharacterData _instanceData;

        bool _canMove = true;
        bool _isDead;

        private Vector2 m_platformVelocity;

        [SerializeField]
        UnityEvent _gameOverEvent;

        [SerializeField]
        UnityEvent _lostLifeEvent;

        [SerializeField]
        UnityEvent _healthChangedEvent;

        [SerializeField]
        CheckpointEvent _checkPointEvent;


        private void Awake()
        {
            _input = GetComponent<EnhancedPlatformer2DUserControl>();
            _audioSource = GetComponent<AudioSource>();
            _character = GetComponent<MyPlatformerCharacter2D>();

            _bodyCollider = gameObject.GetComponent<CapsuleCollider2D>();

            //DontDestroyOnLoad(gameObject);
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (_interactable != null && Input.GetKeyDown(KeyCode.E))
                _interactable.SendMessage("__INTERACT__");

            if (_input.IsAttacking)
            {
                CastAttack();
            }
        }

        private void FixedUpdate()
        {
            if (CanMove())
                _character.Move(_input.Horizontal, _input.IsCrouch, _input.IsJumpingUp, _input.IsJumpingDown, _input.IsAttacking, m_platformVelocity);
            _input.IsJumpingUp = false;
            m_platformVelocity = Vector2.zero;
        }



        private void OnCollisionStay2D(Collision2D collision)
        {
            if (collision.gameObject.tag == "moving-platform")
            {
                m_platformVelocity = collision.rigidbody.velocity;
                m_platformVelocity.y = 0;
            }
        }

        void OnTriggerEnter2D(Collider2D collision)
        {
            //if (collision.gameObject.tag == "coin")
            //{
            //    Debug.Log("COIN!");
            //    Coin coin = collision.gameObject.GetComponent<Coin>();
            //    if (CoinCollected != null)
            //        CoinCollected.Invoke(coin);
            //}

            GameObject collisionGameObject = collision.gameObject;

            if (collisionGameObject.tag == "pass-collider")
            {
                Physics2D.IgnoreCollision(collision.GetComponent<Collider2D>(), _bodyCollider);
            }

            if (collisionGameObject.tag == "interactable")
            {
                _interactable = collision.gameObject;
            }

            if (collisionGameObject.tag == "powerup")
            {
                HealPotion healPotion = collisionGameObject.GetComponent<HealPotion>();
                if (healPotion != null)
                {
                    Heal(healPotion);
                }
            }

            if (collisionGameObject.tag == "checkpoint")
            {
                if (_checkPointEvent != null)
                    _checkPointEvent.Invoke(collisionGameObject);
            }

        }



        void OnTriggerStay2D(Collider2D collision)
        {

        }

        void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.gameObject.tag == "pass-collider")
            {
                Physics2D.IgnoreCollision(collision.GetComponent<Collider2D>(), _bodyCollider, false);

                //foreach (Collider2D c in _bodyCollider)
                //{
                //    Physics2D.IgnoreCollision(collision.GetComponent<Collider2D>(), c, false);
                //}
            }

            if (collision.gameObject.tag == "interactable")
            {
                _interactable = null;
            }
        }

        private void Heal(HealPotion healPotion)
        {
            if (_instanceData.Health + healPotion.HealPoint > _instanceData.MaxHealth)
                _instanceData.Health = _instanceData.MaxHealth;
            else
                _instanceData.Health += healPotion.HealPoint;

            Destroy(healPotion.gameObject);

            RaiseHealthChangedEvent();
        }

        /// <summary>
        /// Indica se il personaggio può essere mosso dal giocatore.
        /// </summary>
        /// <returns></returns>
        private bool CanMove()
        {
            if (_character.IsPlayingState("Hit") || _character.IsPlayingState("Dead") || _character.IsDead())
                _canMove = false;
            else
                _canMove = true;

            return _canMove;
        }

        void CastAttack()
        {
            int HIT_ARRAY_LENGTH = 10;
            RaycastHit2D[] hits = new RaycastHit2D[HIT_ARRAY_LENGTH];

            Vector3 direction = (_character.IsFacingRight) ? transform.right : -transform.right;

            int layerFilter = 1 << LayerMask.NameToLayer("Characters");

            int hitCount = Physics2D.RaycastNonAlloc(transform.position,
                direction,
                hits,
                _instanceData.AttackDistance, layerFilter);

            Debug.DrawLine(transform.position,
                transform.position + direction * _instanceData.AttackDistance,
                Color.red);

            if (hitCount > 0)
            {
                Debug.Log("hits " + hitCount);

                for (int i = 0; i < hitCount; i++)
                {
                    RaycastHit2D rh = hits[i];
                    GameObject hitGo = rh.collider.gameObject;
                    Debug.Log(">" + hitGo.name);
                    if (hitGo.GetInstanceID() != gameObject.GetInstanceID() && hitGo.tag == "enemy")
                    {
                        //Debug.Log("HIT!!!");

                        DamageInfo damage = new DamageInfo();
                        damage.DamagePoint = _instanceData.AttackPoints;
                        hitGo.SendMessage("__TAKE_DAMAGE__", damage);
                    }
                }
            }
        }


        /// <summary>
        /// Invoked when character jump starts.
        /// </summary>
        public void OnCharacterJump()
        {
            _audioSource.PlayOneShot(JumpSound);
        }

        public int GetLifes()
        {
            return _instanceData.Lifes;
        }

        public int GetMaxHealth()
        {
            return _instanceData.MaxHealth;
        }

        public int GetHealth()
        {
            return _instanceData.Health;
        }

        public void __TAKE_DAMAGE__(DamageInfo damageInfo)
        {
            if (damageInfo.InstaKill == true)
            {
                _instanceData.Health = 0;
            }
            else
            {
                _instanceData.Health -= damageInfo.DamagePoint;
            }

            RaiseHealthChangedEvent();

            if (_instanceData.Health > 0)
            {
                _character.GetHit();
            }
            else
            {
                if (!_isDead)
                {
                    _character.Die();
                    _isDead = true;
                    _instanceData.Lifes -= 1;

                    if (_instanceData.Lifes > 0)
                    {
                        if (_lostLifeEvent != null)
                            _lostLifeEvent.Invoke();
                    }
                    else
                    {
                        if (_gameOverEvent != null)
                            _gameOverEvent.Invoke();
                    }

                }
            }
        }

        /// <summary>
        /// Scatena l'evento che l'energia è cambiata.
        /// </summary>
        private void RaiseHealthChangedEvent()
        {
            if (_healthChangedEvent != null)
                _healthChangedEvent.Invoke();
        }

        public void __RESPAWN__()
        {
            Debug.Log("RESPAWN");
            _isDead = false;
            _instanceData.Health = _instanceData.MaxHealth;
            RaiseHealthChangedEvent();
            _character.Revive();

        }
    }
}