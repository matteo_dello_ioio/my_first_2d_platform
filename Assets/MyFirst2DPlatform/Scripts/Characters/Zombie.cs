﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyFirst2DPlatform
{

    public class Zombie : MonoBehaviour
    {
        CircleCollider2D _bodyCollider;
        Animator _animator;

        [SerializeField]
        int _lifePoints;
        [SerializeField]
        bool FlipOnStart;

        /// <summary>
        /// Distanza campo visivo.
        /// </summary>
        [SerializeField]
        float _viewDistance = 2;
        [SerializeField]
        float _viewRadius = 1;

        [SerializeField]
        float _attackDistance = 0.5f;
        [SerializeField]
        int _attackPoints = 1;

        /// <summary>
        /// Tempo dopo il quale il personaggio 
        /// morto scompare dallo schermo.
        /// </summary>
        [SerializeField]
        private float _timeAfterDeath = 1;
        [SerializeField]
        private float _waitSecondsForNextAttack = 5;


        bool _doAttack = true;
        private bool _isFacingRight = true;  // For determining which way the player is currently facing.
        private bool _isDead;
        Coroutine _currentRoutine;

        GameObject _target;


        private void Awake()
        {
            _animator = GetComponent<Animator>();
            _bodyCollider = GetComponent<CircleCollider2D>();
        }

        // Use this for initialization
        void Start()
        {
            if (FlipOnStart)
                Flip();

            _currentRoutine = StartCoroutine(IdleLogic());
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        public void __TAKE_DAMAGE__(DamageInfo damageInfo)
        {
            _lifePoints -= damageInfo.DamagePoint;
            if (_lifePoints > 0)
            {
                _animator.SetTrigger("hit");
            }
            else
            {
                if (!_isDead)
                {
                    SwitchRoutine(DeathLogic());
                }
            }
        }



        protected void Flip()
        {
            // Switch the way the player is labelled as facing.
            _isFacingRight = !_isFacingRight;

            // Multiply the player's x local scale by -1.
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }


        protected void Attack()
        {
            _animator.SetTrigger("attack");
        }


        public void __SHOOT__()
        {
            int HIT_ARRAY_LENGTH = 10;
            RaycastHit2D[] hits = new RaycastHit2D[HIT_ARRAY_LENGTH];

            Vector3 direction = (_isFacingRight) ? transform.right : -transform.right;

            int layerFilter = 1 << LayerMask.NameToLayer("Characters");

            int hitCount = Physics2D.RaycastNonAlloc(transform.position, direction, hits, _attackDistance, layerFilter);

            Debug.DrawLine(transform.position, transform.position + direction * _attackDistance, Color.red);

            if (hitCount > 0)
            {
                Debug.Log("hits " + hitCount);

                for (int i = 0; i < hitCount; i++)
                {
                    RaycastHit2D rh = hits[i];
                    GameObject hitGo = rh.collider.gameObject;
                    Debug.Log(">" + hitGo.name);
                    if (hitGo.GetInstanceID() != gameObject.GetInstanceID() && hitGo.tag == "Player")
                    {
                        Debug.Log("HIT!!!");

                        DamageInfo damage = new DamageInfo();
                        damage.DamagePoint = _attackPoints;
                        hitGo.SendMessage("__TAKE_DAMAGE__", damage);
                    }
                }
            }
        }


        //private void OnGUI()
        //{
        //    if (GUILayout.Button("HIT"))
        //    {
        //        gameObject.SendMessage("__TAKE_DAMAGE__");
        //    }

        //    if (GUILayout.Button("ATTACK"))
        //    {
        //        Attack();
        //    }
        //}

        /// <summary>
        /// Controlla se il giocatore rientra nel campo visivo.
        /// </summary>
        /// <param name="targets"></param>
        /// <returns></returns>
        bool CanSeePlayer(out List<GameObject> targets)
        {
            bool result = false;
            targets = null;

            int layerMask = 1 << LayerMask.NameToLayer("Characters");

            Vector2 origin = transform.position;
            Vector2 direction = ((_isFacingRight) ? transform.right : -transform.right);
            int HIT_ARRAY_LENGTH = 5;
            RaycastHit2D[] hits = new RaycastHit2D[HIT_ARRAY_LENGTH];
            int hitCount = Physics2D.CircleCastNonAlloc(origin, _viewRadius, direction, hits, _viewDistance, layerMask);

            //Debug.Log("TARGETS " + hitCount);

            if (hitCount > 0)
            {
                targets = new List<GameObject>();
                for (int i = 0; i < hitCount; i++)
                {
                    RaycastHit2D rh = hits[i];
                    GameObject hitGo = rh.collider.gameObject;
                    if (hitGo.tag == "Player")
                    {
                        _target = hitGo;

                        targets.Add(hitGo);
                        result = true;
                    }
                }
            }

            return result;
        }

        IEnumerator IdleLogic()
        {
            while (true)
            {
                List<GameObject> targets = null;
                if(CanSeePlayer(out targets))
                {
                    SwitchRoutine(AttackLogic());
                }

                yield return new WaitForSeconds(1);
            }
        }

        IEnumerator AttackLogic()
        {
            _doAttack = true;
            while (_doAttack)
            {
                if (
                    (_target.transform.position.x < gameObject.transform.position.x && _isFacingRight)
                    ||
                    (_target.transform.position.x > gameObject.transform.position.x && !_isFacingRight)
                    )
                    Flip();

                Attack();

                yield return new WaitForSeconds(_waitSecondsForNextAttack);
            }
        }

        IEnumerator DeathLogic()
        {
            _animator.SetTrigger("dead");
            _isDead = true;
            _bodyCollider.enabled = false;
            yield return new WaitForSeconds(_timeAfterDeath);
            Destroy(gameObject);
        }

        void SwitchRoutine(IEnumerator routine)
        {
            if (_currentRoutine != null)
            {
                StopCoroutine(_currentRoutine);
            }
            _currentRoutine = StartCoroutine(routine);
        }

    }
}